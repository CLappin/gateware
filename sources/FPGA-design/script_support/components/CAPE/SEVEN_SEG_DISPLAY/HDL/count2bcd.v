module count2bcd (
    input clk,
    input resetn,
    output [6:0] led_7_seg

);
    
wire count_en;    
wire [3:0] counter;
// 1 Hz pulse enable

pulse_en_1Hz pulse_en_1Hz_0 (
   .clk(clk),
   .resetn(resetn),
   .count_en(count_en)
   );

// Counter instantiation

counter_4_bit counter_4_bit_0  (
    .clk(clk),
    .resetn(resetn),
    .count_en(count_en),
    .counter(counter)
);

bcd bcd_0(
    .clk(clk),
    .counter(counter),
    .led_7_seg(led_7_seg)
);

endmodule