/*
* 4 bit counter with async reset.
*/
module counter_4_bit (
    input clk,
    input resetn,
    input count_en,
    //output led,
    output reg [3:0] counter
);

    //reg [3:0] counter;

    always @( posedge clk or negedge resetn ) 
        begin 
            if (~resetn)
                begin
                    counter <= 4'b0000;
                end
            else if (count_en)
                begin
                    counter <= counter + 1'b1;
                end 
        end    
    
    //assign led = counter[3];
endmodule