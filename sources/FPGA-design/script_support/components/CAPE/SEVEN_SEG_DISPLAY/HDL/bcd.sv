/*
* BCD2SevenSeg (Common Cathode)
*/
module bcd (
    input clk,
    input wire [3:0] counter,
    output reg [6:0] led_7_seg
);

    //reg [3:0] counter;
    reg [6:0] led_7_seg_comb;

    always @( * ) 
        begin 
            case (counter)
                    4'h0: led_7_seg_comb = 7'b1111110;
                    4'h1: led_7_seg_comb = 7'b0110000;
                    4'h2: led_7_seg_comb = 7'b1101101;
                    4'h3: led_7_seg_comb = 7'b1111001;
                    4'h4: led_7_seg_comb = 7'b0110011;
                    4'h5: led_7_seg_comb = 7'b1011011;
                    4'h6: led_7_seg_comb = 7'b1011111;
                    4'h7: led_7_seg_comb = 7'b1110000;
                    4'h8: led_7_seg_comb = 7'b1111111;
                    4'h9: led_7_seg_comb = 7'b1111011;
                    default: led_7_seg_comb = 7'b0000000;
            endcase
        end    
    
    always @(posedge clk) begin
        led_7_seg <= led_7_seg_comb;        
    end
    assign led = counter[3];
endmodule

                    /*4'ha: led_7_seg = 7'b
                    4'hb: led_7_seg = 7'b
                    4'hc: led_7_seg = 7'b
                    4'hd: led_7_seg = 7'b
                    4'he: led_7_seg = 7'b
                    4'hf: led_7_seg = 7'b  */