`timescale 1ns/100ps
module pulse_en_1Hz(
   input    clk,
   input    resetn,
   output   count_en
   );

   
   reg [24:0] counter;
   
   assign count_en = counter[24];

   always@(posedge clk or negedge resetn)
   begin
      if(~resetn)
         begin
            counter <= 'h0;
         end
      else
         begin
            counter <= counter + 1;
         end
   end
endmodule